﻿using UnityEngine;

[CreateAssetMenu]
public class VectorValue : ScriptableObject
{
    #region Public Fields + Properties

    public Vector2 initialValue;

    #endregion Public Fields + Properties
}