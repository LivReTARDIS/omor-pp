﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneTransition : MonoBehaviour
{
#region Fields
    public Vector2 playerPosition;
    public VectorValue playerTransitionPosition;
    public int sceneToLoad;
#endregion


#region Public Methods
    public void OnTriggerEnter2D(Collider2D other)
    {
        GameManager.Main._rooms.Clear();
        playerTransitionPosition.initialValue = playerPosition;
        SceneManager.LoadScene(sceneToLoad);
    }
#endregion
}