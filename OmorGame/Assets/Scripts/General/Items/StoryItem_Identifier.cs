﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;


public class StoryItem_Identifier : MonoBehaviour
{
#region Fields
    public UnityEvent FoundSpecialDrink;
    public Drinks thisDrink;
    private Collider2D _itemCollider;
    [SerializeField] [ShowInInspector] private Item_SOS _item;
#endregion


#region Properies
    public Item_SOS Item { get; private set; }
#endregion


#region Private Methods
    private void Awake()
    {
        _itemCollider = GetComponent<Collider2D>();
        Item = _item;
        FoundSpecialDrink.AddListener(RegisterThis);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Player>())
        {
            FoundSpecialDrink?.Invoke();
            other.GetComponent<Inventory>().AddItem(Item);
            Destroy(gameObject);
        }
    }
    private void RegisterThis()
    {
        FourDrinks.Main.RegisterDrink(thisDrink);
    }
#endregion
}