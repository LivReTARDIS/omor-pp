﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;



[CreateAssetMenu(fileName = "Shot_", menuName = "Item/Potion/Shot", order = 0)]
public class Shot_SOS : Potion_SOS
{
    public enum ShotType
    {
        ChangeAttributeShot,
        ChangeElementalAttributeShot,
        HealLifepointsShot, 
        spawnSomethingShot
    }

    [Title("Shot Settings",TitleAlignment = TitleAlignments.Centered, HorizontalLine = true,Bold = true)]
  
    
    [TabGroup("$tabName"), PropertyOrder(2f), EnumToggleButtons,LabelWidth(100) ]
    public ShotType shotType;
    [TabGroup("$tabName"), PropertyOrder(2.5f)]
    [ShowIf("shotType",ShotType.ChangeAttributeShot)]
    public PlayerAttributes.Attribute  changingAttribute;
    [TabGroup("$tabName"), PropertyOrder(2.5f)]
    [ShowIf("shotType", ShotType.ChangeElementalAttributeShot)]
    public Element changingElement;
    [FormerlySerializedAs("ChangeValueBy")]
    [TabGroup("$tabName"), PropertyOrder(3f)]
    [HideIf("shotType", ShotType.spawnSomethingShot)]
    public float changeValueBy;
    [FormerlySerializedAs("pfbOfSpawningThing")]
    [TabGroup("$tabName"), PropertyOrder(2.5f)]
    [ShowIf("shotType", ShotType.spawnSomethingShot)]
    public GameObject prefabOfSpawningThing;
    [TabGroup("$tabName"), PropertyOrder(3f)]
    [ShowIf("shotType", ShotType.spawnSomethingShot)] 
    public int spawnAmount = 1;

    public override void Deactivate(GameObject player)
    {
        switch (shotType)
        {
            case ShotType.ChangeAttributeShot:
                player.GetComponent<PlayerAttributes>().ChangeAttribute(changingAttribute, -changeValueBy);
                break;
            case ShotType.ChangeElementalAttributeShot:
                player.GetComponent<PlayerAttributes>().ChangeElementMultiplier(changingElement, -changeValueBy);
                break;
            case ShotType.HealLifepointsShot:
                Debug.LogError("There should be nothing to reverse");
                break;
            case ShotType.spawnSomethingShot:
                Debug.LogError("There should be nothing to reverse");
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void ChangeAttribute(GameObject player)
    {
        player.GetComponent<PlayerAttributes>().ChangeAttribute(changingAttribute, changeValueBy);
    }

    private void ChangeElementalAttribute(GameObject player)
    {
        player.GetComponent<PlayerAttributes>().ChangeElementMultiplier(changingElement,changeValueBy);
    }

    private void SpawnSomething(GameObject player)
    {
        for( int i = 0; i<= spawnAmount; i++)
        {
        GameObject clone = Instantiate(prefabOfSpawningThing, player.transform.position, player.transform.rotation);
        }
    }

    private void Heal(GameObject player)
    {
        player.gameObject.GetComponent<ILifeController>().Heal(changeValueBy);
    }


    private void OnEnable()
    {
        switch (shotType)
        {
            case ShotType.ChangeAttributeShot:
                useEvent += ChangeAttribute;
                break;
            case ShotType.ChangeElementalAttributeShot:
                useEvent += ChangeElementalAttribute;
                break;
            case ShotType.HealLifepointsShot:
                useEvent += Heal;
                break;
            case ShotType.spawnSomethingShot:
                useEvent += SpawnSomething;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
   
    }
}