﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;


public abstract class Potion_SOS : Item_SOS
{
    #region Fields
    [HideInInspector] public string tabName = "Potion Values";
    [TabGroup("$tabName")]
    [PropertyOrder(1.006f)]
    [ShowInInspector]
    [AssetList(Path = "/ScriptableObjects/Recepies")]
    public Recipe_SOS recipe;
    [TabGroup("$tabName"), PropertyOrder(1.002f)] [ShowInInspector,ShowIf(nameof(hasRoomDurability))]
    public int roomDurability =-1;
    [TabGroup("$tabName"), PropertyOrder(1.003f)]
    public bool hasRoomDurability;
    #endregion


    #region Public Methods
    /// <summary>
    ///     The potion Effect is used on player
    /// </summary>
    public virtual void Drink(GameObject player)
    {
    }
    #endregion

#region Private Methods
    private void Awake()
    {
        useEvent += Drink;
    }
    #endregion

    public virtual void Deactivate(GameObject player)
    {

    }
    

    //how many roomns can this potion be active;
}