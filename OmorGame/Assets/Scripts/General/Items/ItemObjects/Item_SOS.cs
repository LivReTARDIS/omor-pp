﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;


public abstract class Item_SOS : ScriptableObject
{
#region Fields
    [TabGroup("Item Values")] [PropertyOrder(1.03f)]
    public string Description;
    [TabGroup("Item Values")] [PropertyOrder(1.02f)]
    public string DisplayName;
    [TabGroup("Item Values", Order = -1f)] [PropertyOrder(1.001f)]
    public string ID;
    [TabGroup("Item Values")] [PropertyOrder(1.03f)]
    public int Value;
#endregion


#region Properies
    private bool _stackable { get; set; }
#endregion


#region Public Methods
    public void Display()
    {
        display?.Invoke();
    }
    public void Drop()
    {
        dropEvent?.Invoke();
    }
    public void Use(GameObject user)
    {
        useEvent?.Invoke(user);
    }
#endregion


    protected event Action<GameObject> useEvent;
    protected event Action display;
    protected event Action dropEvent;
}