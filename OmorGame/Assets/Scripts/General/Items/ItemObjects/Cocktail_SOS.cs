﻿using Sirenix.OdinInspector;
using UnityEngine;


[CreateAssetMenu(fileName = "Coctail_", menuName = "Item/Potion/Cocktail", order = 1)]
public class Cocktail_SOS : Potion_SOS
{
#region Fields
    [FoldoutGroup("$tabName", Order = -0.001f)]
    [PropertyOrder(1.004f)]
    [ShowInInspector]
    [AssetList(Path = "Prefabs/Items/Potions/Cocktails")]
    [Required]
    public GameObject Prefab;
    public Drinks Storydrink;
#endregion


#region Public Methods
    public override void Deactivate(GameObject player)
    {
        player.GetComponent<PlayerAttack>().LoadStandart();
    }
    public override void Drink(GameObject player)
    {
        player.GetComponent<PlayerAttack>().LoadWeapon(Prefab);
    }
#endregion


#region Private Methods
    private void OnEnable()
    {
        useEvent += Drink;
    }
#endregion
}