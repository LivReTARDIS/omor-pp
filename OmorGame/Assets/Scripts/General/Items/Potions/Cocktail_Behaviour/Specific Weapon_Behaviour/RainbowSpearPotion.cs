﻿public sealed class RainbowSpearPotion : WeaponBehaviour
{
    #region Public Methods

    public void ReplaceSpear()
    {
        GameObjectMA = Instantiate(prefabMeleeAttack, transform);
        GameObjectMA.transform.parent = gameObject.transform;
    }

    #endregion Public Methods
}