﻿using Sirenix.OdinInspector;

public class SpearAura : AB_Bullet
{
    #region Public Fields + Properties

    [ReadOnly]
    public Element element;

    #endregion Public Fields + Properties
}