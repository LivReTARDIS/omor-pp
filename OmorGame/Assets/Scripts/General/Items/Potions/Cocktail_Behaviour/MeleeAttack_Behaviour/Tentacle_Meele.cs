public class Tentacle_Meele : AB_Meele
{
#region Fields
    private Tentacle_Directional _td;
#endregion


#region Public Methods
    public void TentacleDMG()
    {
        if (_td.isScooting)
        {
            _td.FullUnscoot();
            DMGtoAll(dmg * 1.5f);

            return;
        }

        DMGtoAll(dmg);
    }
#endregion


#region Protected Methods
    protected override void Action()
    {
        TentacleDMG();
    }
#endregion


#region Private Methods
    private void Start()
    {
        _td = GetComponentInParent<Tentacle_Directional>();
    }
#endregion
}