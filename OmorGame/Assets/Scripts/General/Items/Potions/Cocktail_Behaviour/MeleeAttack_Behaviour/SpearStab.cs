﻿using Sirenix.OdinInspector;
using UnityEngine;

public class SpearStab : AB_Meele
{
    #region Public Fields + Properties

    public bool holdingSpear;
    public float cooldownBecauseOfAura;

    #endregion Public Fields + Properties

    #region Private Fields + Properties

    [ShowInInspector, ReadOnly]
    private float _t = 0;

    [ShowInInspector, ReadOnly]
    private bool _allowedToStab = true;

    #endregion Private Fields + Properties

    #region Public Methods

    public void refillTimer()
    {
        _t = cooldownBecauseOfAura;
    }

    #endregion Public Methods

    #region Protected Methods

    protected override void Action()
    {
        //if (!_allowedToStab) return;
        //base.Action();
        //enemies.ForEach(e => e.transform.GetComponent<Rigidbody2D>()
        //.AddForce(PlayerMoveController.directions[transform.parent.parent.GetComponent<PlayerMoveController>().futureDirection] * 100));
        //enemies.ForEach(e => e.transform.GetComponent<GeneralEnemyMovementFollowPlayer>().Moving = false);
        
        /*
         * TODO :
         * fix this 
         */
    }

    protected override void FixedUpdateOperations()
    {
        base.FixedUpdateOperations();
        _t.ToZeroTimer();
        _allowedToStab = (_t == 0 ? true : false);
    }

    #endregion Protected Methods
}