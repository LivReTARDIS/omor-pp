using System;
using System.Collections;
using UnityEngine;


public class Tentacle_Directional : AB_Directional
{
#region Fields
    public GameObject hook;
    public GameObject hookoptik;
    public bool isScooting;
    private GameObject _currentHook;
    private LayerMask laerLayerMask;
    private Rigidbody2D _rb;
    private Vector2 scootdirection;
    private Action FixTest;
    private bool _wasEnemy;
    private StunnAndKnockback _stunnAndKnockback;
#endregion


#region Public Methods
    public void FullUnscoot()
    {
        if (_wasEnemy) _stunnAndKnockback.Stunn(1f);
        Destroy(_currentHook);
        UnScoot();
    }
    public IEnumerator Maxtest()
    {
        while (isScooting)
        {
            Instantiate(hookoptik, transform.position, Quaternion.identity);

            yield return new WaitForSeconds(0.02f);
        }
    }
#endregion


#region Protected Methods
    protected override void FixedUpdateOperations()
    {
        FixTest?.Invoke();
    }
    protected override void Action()
    {
        if (isScooting) return;
        _stunnAndKnockback = null;
        _wasEnemy = false;
        var hit2D = Physics2D.Raycast(transform.position, direction, Mathf.Infinity, laerLayerMask);
        Debug.DrawRay(transform.position, direction, Color.cyan, 1f);
        if (hit2D)
        {
            if (hit2D.transform.gameObject.GetComponent<StunnAndKnockback>() is StunnAndKnockback enemyStunnAndKnockback
            )
            {
                _stunnAndKnockback = enemyStunnAndKnockback;
                _wasEnemy = true;
                _stunnAndKnockback.Stunn();
            }

            player.PlayerMoveControllerCO.moving = false;
            _currentHook = Instantiate(hook, hit2D.point, Quaternion.identity);
            scootdirection = (transform.position - _currentHook.transform.position).normalized *
                             player.PlayerMoveControllerCO.PlayerSpeedMC() *
                             2;

            isScooting = true;
            FixTest = Scoot;
            //StartCoroutine(Maxtest());
        }
    }
#endregion


#region Private Methods
    private void Awake()
    {
        laerLayerMask = LayerMask.GetMask(
            LayerMask.LayerToName(8),
            LayerMask.LayerToName(11),
            LayerMask.LayerToName(21));

        Debug.Log(LayerMask.LayerToName(8));
        Debug.Log(LayerMask.LayerToName(11));
        Debug.Log(LayerMask.LayerToName(21));
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == _currentHook) FullUnscoot();
    }
    private void Scoot()
    {
        _rb.MovePosition(_rb.position - scootdirection * Time.fixedDeltaTime);
    }
    private void Start()
    {
        _rb = player.PlayeRigidbody2D;
    }
    private IEnumerator Test()
    {
        yield return new WaitForSeconds(.5f);
        StartCoroutine(Test());
    }
    private void UnScoot()
    {
        isScooting = false;
        FixTest = null;
        player.PlayerMoveControllerCO.moving = true;
    }
#endregion
}