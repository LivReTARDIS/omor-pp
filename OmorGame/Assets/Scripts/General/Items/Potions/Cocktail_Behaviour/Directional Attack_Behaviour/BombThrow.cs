﻿using UnityEngine;

public sealed class BombThrow : AB_Directional
{
    #region Protected Methods

    protected override void Action()
    {
        clone = Instantiate(prefabBullet, transform.parent.transform.position, transform.parent.transform.rotation);
        clone.GetComponent<AB_Bullet>().direction = direction;
        clone.GetComponent<AB_Bullet>().player = player;
        clone.GetComponent<AB_Bullet>().speed = speed;
        clone.GetComponent<AB_Bullet>().attackTime = attackTime;
        clone.GetComponent<AB_Bullet>().isFlying = true;
    }

    #endregion Protected Methods

    #region Private Methods

    private void Update()
    {
        transform.position = new Vector2(transform.parent.position.x, transform.parent.position.y) + direction * prefabBullet.GetComponent<AB_Bullet>().attackrange;
    }

    #endregion Private Methods
}