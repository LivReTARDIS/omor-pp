﻿using TMPro;
using UnityEngine;

public class UIUpdater : MonoBehaviour
{
    #region Public Fields + Properties

    public TextMeshProUGUI thisTextField;
    public ILifeController lifeController;

    #endregion Public Fields + Properties

    #region Private Methods

    private void Update()
    {
        thisTextField.text = "HP " + lifeController.Lifepoints;
    }

    #endregion Private Methods
}