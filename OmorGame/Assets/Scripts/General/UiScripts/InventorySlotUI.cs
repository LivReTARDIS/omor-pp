using System.Linq;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;


public class InventorySlotUI : MonoBehaviour , ISelectHandler
{
#region Fields
    public Inventory Inventory;
    [ShowInInspector]public InventorySlot InventorySlot;
    public TextMeshProUGUI textCounter;
    public TextMeshProUGUI textName;
    public UnityEvent updateThis;
#endregion


#region Public Methods
    public void Use()
    {
        if (InventorySlot != null) Inventory.Use(InventorySlot);
    }
#endregion


#region Private Methods
    private void Start()
    {
        updateThis.AddListener(UpdateText);
        Inventory = GameManager.Main.GetRegisteredPlayers().First().GetComponent<Inventory>();
    }
    private void UpdateText()
    {
        Debug.Log("why does this work");
        if (InventorySlot == null)
        {
            textCounter.text = "0";
            textName.text = "Omor";

            return;
        }

        textCounter.text = InventorySlot.amount.ToString();
        textName.text = InventorySlot.item.name;
    }

    public void OnSelect(BaseEventData eventData)
    {
        if (eventData.selectedObject == gameObject)
        {
            Debug.Log(gameObject.name + " was selected");
        }
        Debug.Log(gameObject.name + " was selected");
    }

    #endregion
}