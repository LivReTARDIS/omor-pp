﻿using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;


public class UIUpdaterTXT : MonoBehaviour
{
#region Fields
    public TextMeshPro thisTextField;
    [ShowInInspector] protected EnemyLifeController lifeController;
#endregion


#region Private Methods
    private void Awake()
    {
        lifeController = transform.parent.GetComponent<EnemyLifeController>();
    }

    // Start is called before the first frame update
    private void Start() { }

    // Update is called once per frame
    private void Update()
    {
        //Debug.Log(lifeController.Lifepoints);
        thisTextField.text = "HP " + lifeController.Lifepoints;
    }
#endregion
}