using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;


public class InventoryUI : SerializedMonoBehaviour
{
#region Fields
    public GameObject firstInventorySlot;
    [TableMatrix(HorizontalTitle = "X axis", VerticalTitle = "Y axis")] [OdinSerialize]
    public Image[,] images = new Image[2, 4];
    public Inventory playerInventory;
    [OdinSerialize] public Dictionary<Button, Potion_SOS> potioncounter = new Dictionary<Button, Potion_SOS>();
    private int2 _selectedItem;
    private Color save;
    private EventSystem test;
    private InputSystemUIInputModule test1;
    public Button FirstButton;
    public EventSystem UIiinInputModule;
#endregion


#region Public Methods
    public void UpdateText()
    {
        if(playerInventory == null )return;
        foreach (var BPP in potioncounter)
            if (playerInventory.PlayerInventory.Where(p => p.item == BPP.Value).Any())
            {
                Debug.Log( $"i can fill in {BPP.Value}");
                if (BPP.Key.GetComponentInParent<InventorySlotUI>())
                {
                    BPP.Key.GetComponentInParent<InventorySlotUI>().InventorySlot =
                    playerInventory.PlayerInventory.Where(p => p.item == BPP.Value).First(); 
                    BPP.Key.GetComponentInParent<InventorySlotUI>().updateThis.Invoke();
                }

                Debug.Log(BPP.Key);

            }

        //BPP.Key.GetComponentInChildren<TextMesh>().text =
    }
#endregion


#region Private Methods
    private void OnBecameVisible()
    {
        test.firstSelectedGameObject = firstInventorySlot;
        EventSystem.current.SetSelectedGameObject(firstInventorySlot, null);
        UpdateText();
    }
    private void Start()
    {
        test = FindObjectOfType<EventSystem>();
        playerInventory = GameManager.Main.GetRegisteredPlayers()[0].GetComponent<Inventory>();
        test1 = FindObjectOfType<InputSystemUIInputModule>();
        playerInventory.InventoryChanged.AddListener(UpdateText);
        UpdateText();
    }
    private void OnEnable()
    {
        StartCoroutine(SelectContinueButtonLater());
        UpdateText();
    }
    private IEnumerator SelectContinueButtonLater()
    {
        yield return new WaitForEndOfFrame();
         UIiinInputModule.SetSelectedGameObject(null);
         UIiinInputModule.SetSelectedGameObject(FirstButton.gameObject);
         UpdateText();
    }

    #endregion
}