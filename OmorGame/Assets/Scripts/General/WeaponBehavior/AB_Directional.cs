﻿using Sirenix.OdinInspector;
using UnityEngine;


public class AB_Directional : AttackBehaviour
{
#region Fields
    [PropertyOrder(1f)] public Vector2 direction;
    [PropertyOrder(1.5f)] [InlineButton(nameof(SwitchInversion), "Change Inversion")]
    public bool inversed;
    [PropertyOrder(2f)] [ShowIf(nameof(needsBullet))]
    public GameObject prefabBullet;
    protected static bool needsBullet = true;
    protected GameObject clone;
    private int _inversionNumber;
#endregion


#region Protected Methods
    protected override void Action()
    {
        BulletInstantiation();
        LetFly();
    }
    protected void LetFly()
    {
        if (clone == null) return;
        clone.GetComponent<AB_Bullet>().direction = direction * _inversionNumber;
        clone.GetComponent<AB_Bullet>().speed = speed;
        clone.GetComponent<AB_Bullet>().dmg = dmg;
        clone.GetComponent<AB_Bullet>().attackTime = attackTime;
        clone.GetComponent<AB_Bullet>().isFlying = true;
    }
    protected void BulletInstantiation()
    {
        clone = Instantiate(prefabBullet, gameObject.transform.position, gameObject.transform.rotation);
    }
    protected void DirectionToNESW()
    {
        direction = new Vector2(Mathf.Round(direction.x), Mathf.Round(direction.y));
    }
#endregion


#region Private Methods
    private void OnEnable()
    {
        _inversionNumber = inversed ? 1 : -1;
    }
    private void SwitchInversion()
    {
        inversed = !inversed;
        _inversionNumber = -_inversionNumber;
    }
    private void Update()
    {
        Debug.DrawRay(transform.position, direction * 4, Color.green);
    }
#endregion
}