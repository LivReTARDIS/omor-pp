﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class AB_Meele : AttackBehaviour
{
#region Fields
    protected List<GameObject> _damageReciever = new List<GameObject>();
#endregion


#region Public Methods
    public void DMGtoAll(float localdmg, Element e = Element.Normal)
    {
        _damageReciever = _damageReciever.Where(r => r.gameObject != null).ToList();
        switch (e)
        {
            case Element.Normal:
                _damageReciever.ForEach(r => player.DoNormalDamage(r.gameObject, localdmg));

                break;
            case Element.Piercing:
                _damageReciever.ForEach(r => player.DoPiercingDamage(r.gameObject, localdmg));

                break;
            case Element.Explosion:
                _damageReciever.ForEach(r => player.DoExplosionDamage(r.gameObject, localdmg));

                break;
            case Element.Slime:
                _damageReciever.ForEach(r => player.DoSlimeDamage(r.gameObject, localdmg));

                break;
            case Element.Fire:
                _damageReciever.ForEach(r => player.DoFireDamage(r.gameObject, localdmg));

                break;
            case Element.Water:
                _damageReciever.ForEach(r => player.DoWaterDamage(r.gameObject, localdmg));

                break;
            case Element.Ice:
                _damageReciever.ForEach(r => player.DoIceDamage(r.gameObject, localdmg));

                break;
            case Element.Lightning:
                _damageReciever.ForEach(r => player.DoLightningDamage(r.gameObject, localdmg));

                break;
            case Element.Knockback:
                _damageReciever.ForEach(r => player.DoKnockbackDamage(r.gameObject, localdmg));

                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(e), e, null);
        }
    }
#endregion


#region Protected Methods
    protected override void Action()
    {
        DMGtoAll(dmg);
    }
#endregion


#region Private Methods
    private void OnTriggerEnter2D(Collider2D collision)
    {
        _damageReciever.Add(collision.gameObject);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        _damageReciever.Remove(collision.gameObject);
    }
#endregion
}