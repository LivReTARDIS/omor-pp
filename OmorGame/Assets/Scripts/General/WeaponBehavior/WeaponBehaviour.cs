﻿using UnityEngine;
using UnityEngine.InputSystem;


public class WeaponBehaviour : MonoBehaviour
{
#region Fields
    public bool canShoot = true;
    public Player player;
    public GameObject playerGO;
    public GameObject prefabDirectionalAttack;
    public GameObject prefabDodgeAttack;
    public GameObject prefabMeleeAttack;
    private AB_Directional _directionalAttack;
    private AB_Meele _meeleAttack;
    private AB_Dodge _dodgeAttack;
    private bool _canmove;
#endregion


#region Properies
    public GameObject GameObjectDA { protected set; get; }
    public GameObject GameObjectDoA { protected set; get; }
    [HideInInspector] public GameObject GameObjectMA { protected set; get; }
#endregion


#region Public Methods
    public void DetermineDirectionTimed(InputAction.CallbackContext context)
    {
        //   if (_directionalAttack.stopsMovement) player.PlayerMoveControllerCO.DisableMovement();
        // _directionalAttack.direction = context.action.ReadValue<Vector2>();
    }
    public void DirectionalAttackTimed(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            player.animator.SetFloat("moveX", context.action.ReadValue<Vector2>().x);
            player.animator.SetFloat("moveY", context.action.ReadValue<Vector2>().y);
            _directionalAttack.direction = context.action.ReadValue<Vector2>(); 
            _directionalAttack.Attack();
        }
    }
    public void DodgeAttack(InputAction.CallbackContext context)
    {
        if (context.canceled) _dodgeAttack.Attack();
    }
    public void MeeleAttackTimed(InputAction.CallbackContext context)
    {
        if (_meeleAttack is AB_MeeleCharged meeleAttackCharged)
        {
            meeleAttackCharged.GiveCharge(context.duration);
            player.PlayerMoveControllerCO.DisableMovement();
        }

        if (context.canceled) _meeleAttack.Attack();
    }
#endregion


#region Protected Methods
    protected virtual void Awake()
    {
        Initialization();
    }
#endregion


#region Private Methods
    private void Initialization()
    {
        playerGO = gameObject.GetComponentInParent<Player>().gameObject;
        player = playerGO.GetComponent<Player>();
        GameObjectDA = PrefabCheck(prefabDirectionalAttack);
        GameObjectMA = PrefabCheck(prefabMeleeAttack);
        GameObjectDoA = PrefabCheck(prefabDodgeAttack);
        _directionalAttack = GameObjectDA.GetComponent<AB_Directional>();
        _meeleAttack = GameObjectMA.GetComponent<AB_Meele>();
        _dodgeAttack = GameObjectDoA.GetComponent<AB_Dodge>();
        _directionalAttack.player = player;
        _meeleAttack.player = player;
        _dodgeAttack.player = player;
    }
    private GameObject PrefabCheck(GameObject prefab)
    {
        if (prefab != gameObject) return Instantiate(prefab, transform);

        return gameObject;
    }
#endregion


    /// <summary>
    /// counts doen time untill its zero should to be in fixdeupdate ;
    /// </summary>
    /// <param name="temptime"></param>
    /// <returns></returns>
}