using System;
using Sirenix.OdinInspector;
using UnityEngine;


public class AB_MeeleCharged : AB_Meele
{
    public enum damageFormula
    {
        liniear,
        quadratisch,
        exponantial
    }


#region Fields
    [Tooltip("Damage in Attackvalues shows the calculated dmg")]
    public float MaxDamage;
    public float MaxTime;
    public float MinDamage;
    private Func<float, float> DamageCalculation;
    private damageFormula _currentDamageFormula;
#endregion


#region Properies
    /*TODO:
     * -f�r gamedesigner hier alles aufr�umen
     * - nicht sachen anzeigen und an unterschiedlichen stellen unterschiedlich beuntezn
     */
    [ProgressBar(0f, nameof(MaxTime))]
    [ShowInInspector]
    public double CurrentCharge { get; private set; }
    [ShowInInspector]
    public damageFormula DamageFormula
    {
        get => _currentDamageFormula;
        set
        {
            if (_currentDamageFormula == value) return;
            {
                _currentDamageFormula = value;
                switch (value)
                {
                    case damageFormula.liniear:
                        DamageCalculation = LinearCalculation;

                        break;
                    case damageFormula.quadratisch:
                        throw new ArgumentOutOfRangeException(nameof(value), value, null);

                        break;
                    case damageFormula.exponantial:
                        throw new ArgumentOutOfRangeException(nameof(value), value, null);

                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(value), value, null);
                }
            }
        }
    }
#endregion


#region Public Methods
    public void GiveCharge(double time)
    {
        CurrentCharge = time;
    }
#endregion


#region Protected Methods
    protected override void Action()
    {
        dmg = LinearCalculation((float) CurrentCharge);
        DMGtoAll(dmg);
        player.PlayerMoveControllerCO.EnableMovement();
    }
#endregion


#region Private Methods
    private float LinearCalculation(float duration)
    {
        return Mathf.Min(MaxDamage, (MaxDamage - MinDamage / MaxTime) * duration + MinDamage);
    }
#endregion
}