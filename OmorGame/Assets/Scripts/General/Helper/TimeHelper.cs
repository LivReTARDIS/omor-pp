﻿using UnityEngine;

public static class TimeHelper
{
    #region Public Methods

    public static bool ToZeroTimer(this ref float temptime)
    {
        temptime = temptime - Time.fixedDeltaTime;
        if (temptime <= 0)
        {
            temptime = 0;
            return true;
        }

        return false;
    }

    #endregion Public Methods
}