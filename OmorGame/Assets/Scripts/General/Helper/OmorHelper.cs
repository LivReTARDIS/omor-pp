using System.Collections;
using UnityEngine;


public static class OmorHelper
{
#region Public Methods
    public static void DeastroyAfterTime(this GameObject go, float waitTime, MonoBehaviour mono)
    {
        mono.StartCoroutine(DeastroyAfterTimeRoutine(go, waitTime));
    }
    public static bool IsOverCanyon(this GameObject gameObject)
    {
        var RaycastHit = Physics2D.Raycast(
            new Vector3(
                gameObject.transform.position.x,
                gameObject.transform.position.y,
                gameObject.transform.position.z - 2),
            Vector3.forward * 4,
            Mathf.Infinity,
            LayerMask.GetMask(LayerMask.LayerToName(22)));

        return RaycastHit.transform != null;
    }
#endregion


#region Private Methods
    private static IEnumerator DeastroyAfterTimeRoutine(GameObject go, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Object.Destroy(go);
    }
#endregion
}