using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Sirenix.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UsePotionUI : SerializedMonoBehaviour
{
#region Fields
    public Image[] Images  = new Image[4];
    public Image[] Borders = new Image[4];
    public Drinks[] drinks = new Drinks[4];
    private Player player;
    public TextMeshProUGUI timer;


    public Color active;
    public Color hidden;
    public Color notAvailable;
    private void Start()
    {
        Images.ForEach(i => i.color = hidden);
        player = GameManager.Main.GetRegisteredPlayers().First().GetComponent<Player>();
        UpdateImages();
        FourDrinks.Main.FoundADrink.AddListener(UpdateImages);
        player.UsePotions.timerChanged.AddListener(UpdateTimer);
    }

    private void UpdateImages()
    {
        int i = 0;
        foreach (var variable in drinks)
        {
            if (FourDrinks.Main.ReturnFoundDrinks().Contains(variable))
            {
                 Images[i].color= Color.white;
            }
            i++;
        }
    }

    public void Highlight(Drinks drink)
    {
        int i = 0;
        foreach (var variable in drinks)
        {
            if (variable == drink)
            {
                Borders[i].color = active;
                return;
            }
            Borders[i].color = hidden;
            i++;
        }

    }

    public void UpdateTimer()
    {
        timer.text = $"{player.UsePotions.Wainttime}";
    }

    public void AllDeactivated()
    {
        Images.ForEach(i => i.color = notAvailable);
        Borders.ForEach(b => b.color = notAvailable);
    }

    public void AllActivated()
    {
        Images.ForEach(i => i.color = Color.white);
        Borders.ForEach(b => b.color = Color.white);
    }

    #endregion
}