using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;


public class PlayerMoveController : SerializedMonoBehaviour
{
#region Fields
    public Vector2 currentCardinal;
    public static Vector2[] directions = {Vector2.up, Vector2.down, Vector2.right, Vector2.left};
    [HideInInspector] public int futureDirection;
    public Vector2 movement;
    public bool moving = true;
    public Player player;
    [OnValueChanged(nameof(PlayerSpeed))] public float range;
    [OnValueChanged(nameof(PlayerSpeed))] public float time;
    private float _time;
    private Rigidbody2D _rb;
    private Transform _t;
    [ShowInInspector] [ReadOnly] private float _prozessecedSpeed;
    private float _range;
#endregion


#region Public Methods
    public void DisableMovement()
    {
        moving = false;
    }
    public void EnableMovement()
    {
        moving = true;
    }
    public void Moving(InputAction.CallbackContext context)
    {
        if (context.action.ReadValue<Vector2>() != currentCardinal &&
            context.action.ReadValue<Vector2>() != Vector2.zero &&
            (
                Mathf.Abs(context.action.ReadValue<Vector2>().x) == 1 ||
                Mathf.Abs(context.action.ReadValue<Vector2>().y) == 1))
            currentCardinal = context.action.ReadValue<Vector2>();

        movement = context.action.ReadValue<Vector2>() * _prozessecedSpeed;
        if (!moving)
        {
            player.animator.SetBool("moving", false);

            return;
        }

        player.animator.SetBool("moving", movement != Vector2.zero);

        if (movement == Vector2.zero) return;
        player.animator.SetFloat("moveX", movement.x);
        player.animator.SetFloat("moveY", movement.y);
        player.animator.SetFloat("movementSpeed", _prozessecedSpeed / 2);
    }
    public void PlayerSpeed()
    {
        if (player == null || player.PlayerAttributesCO == null) return;
        _prozessecedSpeed = player.PlayerAttributesCO.ApplySpeedMultiplier(range / time);
    }
    public float PlayerSpeedMC()
    {
        PlayerSpeed();

        return _prozessecedSpeed;
    }
#endregion


#region Private Methods
    private void Awake()
    {
        _rb = gameObject.GetComponent<Rigidbody2D>();
        _t = transform;
        currentCardinal = Vector2.zero;
    }
    private void FixedUpdate()
    {
        //  moving = (movement != Vector2.zero);
        if (!moving) return;
        _rb.MovePosition(_rb.position + movement * Time.fixedDeltaTime);
    }
    private void Start()
    {
        PlayerSpeed();
        player.PlayerAttributesCO.SpeedMultiplierChanged += PlayerSpeed;
    }
#endregion
}