﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerAttack : MonoBehaviour
{

    #region Fields
    public GameObject standartWeapon;

    [AssetList(Path = "Prefabs/Items/Potions/Cocktails/Weapons")] [InlineEditor(InlineEditorModes.LargePreview)]
    public GameObject weaponPotion;

    private GameObject _go_Active_WeaponPotion;
    private WeaponBehaviour _sc_Active_WeaponPotion;
    #endregion

    #region Methods
    private void Start()
    {
        LoadWeapon(weaponPotion);
    }

    public void LoadWeapon(GameObject newWeapon)
    {
        if (_go_Active_WeaponPotion != null) Destroy(_go_Active_WeaponPotion);
        _go_Active_WeaponPotion = Instantiate(newWeapon, gameObject.transform);
        _sc_Active_WeaponPotion = _go_Active_WeaponPotion.GetComponent<WeaponBehaviour>();
        _sc_Active_WeaponPotion.playerGO = gameObject;
    }

    public void MeleeAttack(InputAction.CallbackContext context)
    {
        _sc_Active_WeaponPotion.MeeleAttackTimed(context);
    }

    public void DirectionalAttack(InputAction.CallbackContext context)
    {
        _sc_Active_WeaponPotion.DirectionalAttackTimed(context);
    }

    public void DetermineDirection(InputAction.CallbackContext context)
    {
        _sc_Active_WeaponPotion.DetermineDirectionTimed(context);
    }

    public void DodgeAttack(InputAction.CallbackContext context)
    {
        _sc_Active_WeaponPotion.DodgeAttack(context);
    }

    public void LoadStandart()
    {
        LoadWeapon(standartWeapon);
    }
    #endregion

}