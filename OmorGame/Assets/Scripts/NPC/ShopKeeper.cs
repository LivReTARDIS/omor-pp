using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;


public class ShopKeeper : Socializeable
{
#region Fields
    public GameObject pfbSchild;
    [OdinSerialize] [DictionaryDrawerSettings(KeyLabel = "Spawnpoint", ValueLabel = "Drink")]
    public Dictionary<GameObject, GameObject> shopSpotsy = new Dictionary<GameObject, GameObject>();
    private Item_SOS _item;
    private readonly List<GameObject> spawnedDrinks = new List<GameObject>();
#endregion


#region Public Methods
    public void Buy(GameObject player)
    {
        CostCheck(player.GetComponent<Inventory>());
    }
    public void SetBuyingObject(Item_SOS item)
    {
        _item = item;
    }
    public IEnumerator wait()
    {
        yield return new WaitForSeconds(1f);
        StopTalking();
    }
#endregion


#region Private Methods
    private void Awake()
    {
        Instanciiate();
        FourDrinks.Main.FoundADrink.AddListener(Instanciiate);
    }
    private void CostCheck(Inventory inventory)
    {
        StopAllCoroutines();
        if (inventory.RemoveCoins(3))
        {
            SaySomething(0);
            inventory.AddItem(_item);
            StartCoroutine(wait());

            return;
        }

        SaySomething(1);
        StartCoroutine(wait());
    }
    [Button]
    private void Instanciiate()
    {
        spawnedDrinks.ForEach(g => Destroy(g));
        spawnedDrinks.Clear();
        foreach (var o in shopSpotsy)
        {
            GameObject clone;
            if (o.Value.GetComponent<ShopItem>()._item is Cocktail_SOS cocktail)
                if (!FourDrinks.Main.HasFoundDrink(cocktail.Storydrink))
                {
                    clone = Instantiate(pfbSchild, o.Key.transform.position, Quaternion.identity, transform);
                    spawnedDrinks.Add(clone);

                    continue;
                }

            clone = Instantiate(o.Value, o.Key.transform.position, Quaternion.identity, transform);
            spawnedDrinks.Add(clone);
        }
    }
#endregion
}