﻿using UnityEngine;


public class Singelton<T> : MonoBehaviour where T : Component
{
#region Properies
    public static T Main { get; set; }
#endregion


#region Private Methods
    private void Awake()
    {
        if (Main == null)
        {
            Main = this as T;
        }
        else if (Main != this)
        {
            Destroy(Main);
            Main = this as T;
        }

        DontDestroyOnLoad(gameObject);
    }
#endregion
}