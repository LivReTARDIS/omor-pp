using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;


public class PlayerUIController : MonoBehaviour
{
#region Fields
    public Player player;
    private GameObject _UICanvas;
    private UIManager _uiManager;
    private bool freshSwitch;
    private bool isUiOpen;
#endregion


#region Public Methods
    public void CloseInventory(InputAction.CallbackContext context)
    {
   
        //player.playerInput.StopAllCoroutines();

        if (!isUiOpen) return;
        if (freshSwitch) return;
        if (context.performed)
        {
            player.playerInput.uiInputModule.enabled = true;
            isUiOpen = false;
            player.playerInput.uiInputModule.enabled = false;
            player.playerInput.SwitchCurrentActionMap("Player");
            _uiManager.CloseInventory();
            //Debug.Log("I changed to " +player.playerInput.currentActionMap);
            StartCoroutine(wait());
        }
    }
    public void OpenInventory(InputAction.CallbackContext context)
    {

        if (isUiOpen) return;
        if (freshSwitch) return;
        if (context.performed)
        {
            isUiOpen = true;
            player.playerInput.uiInputModule.enabled = true;
            player.playerInput.SwitchCurrentActionMap("UI");
            StartCoroutine(wait());
            _uiManager.OpenInventory();
          //  Debug.Log("I changed to " +player.playerInput.currentActionMap);
        }
    }
#endregion


#region Private Methods
    private void Start()
    {
        _UICanvas = GameManager.Main.uiCanvas;
        _uiManager = UIManager.Main;
    }
    private void Update() { }
    private IEnumerator wait()
    {
        freshSwitch = true;

        yield return new WaitForSeconds(0.2f);
        freshSwitch = false;
    }

    public void NavigateLeft()
    {
    }

#endregion
}