using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class FourDrinks : Singelton<FourDrinks>
{
#region Fields
    public UnityEvent FoundADrink;
    public UnityEvent FoundAllDrinks;
    private readonly int _tentacleBitmask = 1 << 0;
    private readonly int _explodeBitmask = 1 << 1;
    private readonly int _flashMobBitmask = 1 << 2;
    private readonly int _stickyBitmask = 1 << 3;
    private readonly int hasfoundall = 15;
    private int foundDrinks;
#endregion


#region Public Methods
    public void AllFoundCheck()
    {
        if (hasfoundall == foundDrinks)
        {
            FoundAllDrinks?.Invoke();
        }
    }
    public bool CheckBitmask(int bitmask)
    {
        return (foundDrinks & bitmask) == bitmask;
    }
    public bool HasFoundDrink(Drinks drink)
    {
        var bitmask = 1 << 5;
        switch (drink)
        {
            case Drinks.beer:
                bitmask = _tentacleBitmask;

                break;
            case Drinks.explode:
                bitmask = _explodeBitmask;

                break;
            case Drinks.flashMob:
                bitmask = _flashMobBitmask;

                break;
            case Drinks.sticky:
                bitmask = _stickyBitmask;

                break;
        }

        return CheckBitmask(bitmask);
    }
    public void RegisterDrink(Drinks drink)
    {
        switch (drink)
        {
            case Drinks.beer:
                ApplyBitmast(_tentacleBitmask);

                break;
            case Drinks.explode:
                ApplyBitmast(_explodeBitmask);

                break;
            case Drinks.flashMob:
                ApplyBitmast(_flashMobBitmask);

                break;
            case Drinks.sticky:
                ApplyBitmast(_stickyBitmask);

                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(drink), drink, null);
        }

        FoundADrink?.Invoke();
        AllFoundCheck();
    }
    public List<Drinks> ReturnFoundDrinks()
    {
        var output = new List<Drinks>();
        for (var i = 0; i < Enum.GetValues(typeof(Drinks)).Length; i++)
            if (HasFoundDrink((Drinks) i))
                output.Add((Drinks) i);
        return output;
    }
#endregion


#region Private Methods
    private void ApplyBitmast(int bitmask)
    {
        foundDrinks |= bitmask;
    }
#endregion
}
public enum Drinks
{
    beer = 0,
    explode = 1,
    flashMob = 2,
    sticky = 3,
    non = 4
}