﻿using UnityEngine;


[CreateAssetMenu(fileName = "EnemyWave", menuName = "ScriptableObjects/Waves", order = 4)]
public class EnemyWave : ScriptableObject
{
#region Fields
    public float spawnInterval;
    public GameObject[] typeOfEnemies;
    public string waveName;
#endregion
}