﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;


public class GeneralEnemyMovementFollowPlayer : EnemyMovementController
{
#region Fields
    public GameObject GO_Player;
    [Range(1, 50)] public float playerDetectionRange = 1;
    private Vector2 _direction;
    private Vector2 _movement;
    private bool _moving;
    private List<GameObject> _players;
    [ShowInInspector] private Rigidbody2D _GO_Player_rigidbody2D;
    private Rigidbody2D _this_Rigidbody2D;
    public Vector2 savepos;
#endregion


#region Properies
    [ReadOnly]
    [ShowInInspector]
    public bool Moving
    {
        get => _moving;
        set
        {
            if (_moving == value) return;
            _moving = value;
            enemy.animator.SetBool("Moving",value);
            if (_players.Count == 0) return;
            GetNewRidgidbody();
        }
    }
#endregion


#region Public Methods
    public void DisableMovement()
    {
        Moving = false;
    }
    public void EnableMovemen()
    {
        Moving = true;
    }
#endregion


#region Private Methods
    private void Awake()
    {
        _this_Rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
        _players = GameManager.Main.GetRegisteredPlayers();
        savepos = _this_Rigidbody2D.position;
    }
    private void FixedUpdate()
    {
        if (Moving)
        {
            _direction = _GO_Player_rigidbody2D.position - _this_Rigidbody2D.position;
            _movement = CardinalDirections8.Cardinalize(_direction);
            _movement.Normalize();
            enemy.animator.SetFloat("move x", _movement.x );
            enemy.animator.SetFloat("move y", _movement.y );
            enemy.animator.SetFloat("moving Speed", movingSpeed );
            _this_Rigidbody2D.MovePosition(_this_Rigidbody2D.position + _movement * movingSpeed * Time.fixedDeltaTime);
        }




        Moving = _players
            .Where(d => Vector3.Distance(transform.position, d.gameObject.transform.position) <= playerDetectionRange)
            .Any(d => d);

         enemy.animator.SetBool("Moving", (_this_Rigidbody2D.position != savepos));
         savepos = _this_Rigidbody2D.position;
    }

    private void GetNewRidgidbody()
    {
        if (!_moving) return;
        _GO_Player_rigidbody2D = _players
            .Where(
                d => Vector3.Distance(transform.position, d.gameObject.transform.position) <=
                     playerDetectionRange)
            .OrderBy(d => Vector3.Distance(transform.position, d.gameObject.transform.position))
            .ToList()
            .First()
            .gameObject
            .GetComponent<Rigidbody2D>();
    }
#endregion
}