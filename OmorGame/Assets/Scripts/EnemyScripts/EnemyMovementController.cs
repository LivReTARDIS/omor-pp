using Sirenix.OdinInspector;
using UnityEngine;


public class EnemyMovementController : MonoBehaviour
{
#region Fields
    [ReadOnly] [ShowInInspector] public bool _isMoving;
    public Enemy enemy;
    public float SpeedMultiplier;
    [ShowInInspector] [SerializeField] private float _movingSpeed;
#endregion


#region Properies
    public bool IsMoving
    {
        get => _isMoving;
        set
        {
            if (value != _isMoving)
            {
                _isMoving = value;
                enemy.animator.SetBool("moving", value);
            }
        }
    }
    [ShowInInspector]
    [ReadOnly]
    public float movingSpeed
    {
        get => _movingSpeed * SpeedMultiplier;
        set => _movingSpeed = value;
    }
#endregion
}