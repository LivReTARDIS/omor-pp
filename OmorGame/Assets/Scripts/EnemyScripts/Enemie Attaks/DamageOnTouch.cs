using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnTouch : MonoBehaviour
{

    public int dmg;
    public float cooldown;
    public List<GameObject> dmgreciever = new List<GameObject>();
    public Enemy enemy;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent(typeof(ILifeController)) && !dmgreciever.Contains(other.gameObject))
        {
            dmgreciever.Add(other.gameObject);
            StartCoroutine(DamageOverTime(other.gameObject));
        }
    }
    private IEnumerator DamageOverTime(GameObject reciever)
    {
        ILifeController lc = reciever.gameObject.GetComponent<ILifeController>();
        
        while (dmgreciever.Contains(reciever.gameObject))
        {
            enemy.animator.SetTrigger("Attack");
            enemy.enemyRigidbody2D.constraints = RigidbodyConstraints2D.FreezeAll;
            yield return  new WaitForSeconds(enemy.animator.GetCurrentAnimatorClipInfo(0).Length/2);
                lc.DoDmg(dmg);
            yield return new WaitForSeconds(enemy.animator.GetCurrentAnimatorClipInfo(0).Length / 2);
            enemy.enemyRigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation;
            yield return new WaitForSeconds(cooldown);
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (dmgreciever.Contains(other.gameObject))
        {
            dmgreciever.Remove(other.gameObject);
        }
    }
}
