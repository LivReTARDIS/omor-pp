﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.Utilities;
using UnityEngine;


public class EnemieStandartMeele : AB_Meele
{
#region Protected Methods
    protected override void Action()
    {
        DMGtoPlayer(_damageReciever);
    }
#endregion


#region Private Methods
    private void DMGtoPlayer(List<GameObject> damagerecievers)
    {
        damagerecievers.Where(d => d != null).ForEach(d => d.GetComponent<ILifeController>().DoDmg(dmg));
    }
#endregion
}