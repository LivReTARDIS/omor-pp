using System;
using Sirenix.OdinInspector;
using UnityEngine.Events;


public class StabStabWoof : Enemy
{
#region Fields
    public StabStabWoofStatus currentStatus;
    public int StabNumber;
    public StabStabWoofStatus StartingStatus;
    public UnityEvent statusIdleEvent;
    public UnityEvent statusMovingEvent;
    public UnityEvent statusPanikEvent;
    public UnityEvent statusStab1Event;
    public UnityEvent statusStab2Event;
    public UnityEvent statusSteppingEvent;
    public UnityEvent statusWoofingEvent;
    public StabStabWoof_Attack Weapon;
    private UnityEvent _event;
#endregion


#region Public Methods
    public void ChangeStatus(StabStabWoofStatus newStatus)
    {
        switch (newStatus)
        {
            case StabStabWoofStatus.Idle:
                statusIdleEvent?.Invoke();

                break;
            case StabStabWoofStatus.Stab1:
                animator.SetTrigger("Attack");
                statusStab1Event?.Invoke();

                break;
            case StabStabWoofStatus.Stab2:
                animator.SetTrigger("Attack");
                statusStab2Event?.Invoke();

                break;
            case StabStabWoofStatus.Stepping:
                statusSteppingEvent?.Invoke();

                break;
            case StabStabWoofStatus.Moving:
                statusMovingEvent?.Invoke();

                break;
            case StabStabWoofStatus.Woof:
                statusWoofingEvent?.Invoke();

                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newStatus), newStatus, null);
        }

        currentStatus = newStatus;
    }
    [Button]
    public void DebugChange()
    {
        ChangeStatus(StartingStatus);
    }
    [Button]
    public void DebugFreeze()
    {
        gameObject.GetComponent<StabStabWoof_Movement>().IsMoving = false;
        AIpaused = true;
    }
    public void DebugUnFreeze()
    {
        AIpaused = false;
        gameObject.GetComponent<StabStabWoof_Movement>().IsMoving = true;
    }
#endregion


#region Private Methods
    private void Start()
    {
        ChangeStatus(StartingStatus);
        Weapon.enemy = this;
    }
#endregion
}
public enum StabStabWoofStatus
{
    Idle,
    Stab1,
    Stab2,
    Stepping,
    Woof,
    Moving
}