using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = System.Random;


public class StabStabWoof_Movement : EnemyMovementController
{
#region Fields
    public float attackrange; //when will he start stabbing 
    public float CooldownForSecondStab;
    public bool freeze;
    public GameObject GO_Player;
    [Range(1, 50)] public float playerDetectionRange = 1;
    public float speed = 1f;
    private StabStabWoof _stabStabWoof;
    private Vector3 _targetPoint;
    private GameObject _targetObject;
    private Vector2 _direction;
    private Rigidbody2D this_rigidbody2D;
    private int a = 1;
    private Random r = new Random(62);
    private int _sattisfactionAttempts;
    private bool _isTryingToSatisfy;
    private Vector3 _playerPositionFixed;
    private float _helptimer = 4f;
    private Vector2 _movement;
    private bool _ht;
    private List<GameObject> _players;
    [ShowInInspector] private Rigidbody2D _GO_Player_rigidbody2D;
    private Rigidbody2D _this_Rigidbody2D;
#endregion


#region Private Methods
    private void Awake()
    {
        _this_Rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
        _players = GameManager.Main.GetRegisteredPlayers();
        IsMoving = false;
        _stabStabWoof = GetComponent<StabStabWoof>();
        this_rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
    }
    private void FixedUpdate()
    {
        enemy.animator.SetFloat("move x", _direction.x);
        enemy.animator.SetFloat("move y", _direction.y);
        if (_players
                .Where(
                    d => Vector3.Distance(transform.position, d.gameObject.transform.position) <= playerDetectionRange)
                .Any(d => d) &&
            _stabStabWoof.currentStatus == StabStabWoofStatus.Idle)
            _stabStabWoof.ChangeStatus(StabStabWoofStatus.Moving);

        if (!IsMoving) return;
        this_rigidbody2D.MovePosition(this_rigidbody2D.position + _direction * movingSpeed * Time.fixedDeltaTime);
        enemy.animator.SetFloat("moving speed", movingSpeed / 2);

        if (_stabStabWoof.currentStatus != StabStabWoofStatus.Moving) return;
        _direction = (_GO_Player_rigidbody2D.position - this_rigidbody2D.position).normalized;
        //  _helptimer -= Time.fixedDeltaTime;
        if (Vector3.Distance(transform.position, _GO_Player_rigidbody2D.position) < attackrange)
        {
            Debug.Log("ahhhhhhhhhhhhhhhhh");
            _stabStabWoof.ChangeStatus(StabStabWoofStatus.Stab1);
        }
    }
    private void GetNewRidgidbody()
    {
        //if (!_ht) return;
        _GO_Player_rigidbody2D = _players
            .Where(
                d => Vector3.Distance(transform.position, d.gameObject.transform.position) <=
                     playerDetectionRange)
            .OrderBy(d => Vector3.Distance(transform.position, d.gameObject.transform.position))
            .ToList()
            .First()
            .gameObject
            .GetComponent<Rigidbody2D>();
    }
    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.layer == 9) _stabStabWoof.ChangeStatus(StabStabWoofStatus.Stab1);
    }
    private IEnumerator WaitToStab()
    {
        yield return new WaitWhile(() => _stabStabWoof.AIpaused);
        _direction = (_GO_Player_rigidbody2D.position - this_rigidbody2D.position).normalized;

        //Instantiate(new GameObject("test"), _direction, transform.rotation);
        yield return new WaitWhile(() => _stabStabWoof.AIpaused);
        IsMoving = true;

        yield return new WaitForSeconds(CooldownForSecondStab);
        yield return new WaitWhile(() => _stabStabWoof.AIpaused);
        _stabStabWoof.ChangeStatus(StabStabWoofStatus.Stab2);
        HasTarget = true;
    }
    private IEnumerator Woofwait(float waitTime, StabStabWoofStatus folgeStatus)
    {
        yield return new WaitWhile(() => _stabStabWoof.AIpaused);
        _direction = -(_GO_Player_rigidbody2D.position - _this_Rigidbody2D.position).normalized;
        IsMoving = true;

        yield return new WaitWhile(() => _stabStabWoof.AIpaused);
        yield return new WaitForSeconds(0.1f);
        yield return new WaitWhile(() => _stabStabWoof.AIpaused);
        IsMoving = false;

        yield return new WaitWhile(() => _stabStabWoof.AIpaused);
        yield return new WaitForSeconds(waitTime);
        yield return new WaitWhile(() => _stabStabWoof.AIpaused);
        _stabStabWoof.ChangeStatus(folgeStatus);
    }
#endregion


#region Properies
    public bool HasTarget
    {
        get => _ht;
        set
        {
            if (_ht == value) return;
            _ht = value;

            if (_players.Count == 0) return;
            GetNewRidgidbody();
        }
    }
    [ReadOnly]
    [ShowInInspector]
#endregion


#region Public Methods
    public void DisableMovement()
    {
        IsMoving = false;
    }
    public void EnableMovemen()
    {
        IsMoving = true;
    }
    public void Moving()
    {
        GetNewRidgidbody();
        IsMoving = true;
    }
    /*TODO :
     * - dont pick the same seed 
     *
     */
    public void Step()
    {
        StartCoroutine(WaitToStab());
    }
    public void Woof()
    {
        StartCoroutine(Woofwait(2f, StabStabWoofStatus.Idle));
    }
#endregion
}