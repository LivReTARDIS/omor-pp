using System;
using UnityEngine;
using Random = System.Random;


public class SpawnCoin : MonoBehaviour
{
#region Fields
    public Enemy enemy;
    public GameObject pfbCoin;
    private RoomManager _room;
    private readonly Random r = new Random();
#endregion


#region Public Methods
    public void SpawnCoins()
    {
        if (_room == null)
        {
            if (GetChance(0.5f)) Spawn();

            return;
        }

        switch (_room.CurrentRoomState)
        {
            case RoomState.friendly:
                if (GetChance(_room.normalMoneyDropChance)) Spawn();

                break;
            case RoomState.unfriendly:
                if (GetChance(_room.normalMoneyDropChance)) Spawn();

                break;
            case RoomState.rushhour:
                if (GetChance(_room.rushHourMoneyDropChance)) Spawn();

                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
#endregion


#region Private Methods
    private bool GetChance(double chance)
    {
        return chance >= r.NextDouble();
    }
    private void Spawn()
    {
        var clone = Instantiate(
            pfbCoin,
            new Vector3(
                transform.position.x + (float) r.NextDouble(),
                transform.position.y + (float) r.NextDouble(),
                0),
            Quaternion.identity);

        if (clone.IsOverCanyon()) clone.DeastroyAfterTime(1f, this);
    }
    private void Start()
    {
        if (enemy.Spawner == null) return;
        _room = enemy.Spawner.parentRoom;
    }
#endregion
}