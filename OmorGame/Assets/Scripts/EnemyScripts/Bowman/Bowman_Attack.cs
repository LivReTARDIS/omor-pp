using System.Collections;
using System.Linq;
using UnityEngine;


public class Bowman_Attack : EnemyRangeStandart
{
#region Fields
    private Bowman _bowman;
    private GameObject _player;
#endregion


#region Public Methods
    public void Shoot()
    {
        _player = GameManager.Main.GetRegisteredPlayers().First();
        StartCoroutine(ShootCooroutine());
    }
#endregion


#region Private Methods
    private void Awake()
    {
        _bowman = gameObject.GetComponent<Bowman>();
        inversed = false;
    }
    private IEnumerator ShootCooroutine()
    {
        _player = GameManager.Main.GetRegisteredPlayers().First();
        var shots = 3;
        while (shots > 0)
        {
            yield return new WaitWhile(() => _bowman.AIpaused);
            _bowman.animator.SetTrigger("Attack");

            yield return new WaitForSeconds(_bowman.animator.GetCurrentAnimatorClipInfo(0).Length);
            // direction = CardinalDirections8.Cardinalize(_player.transform.position.normalized);
            direction = (transform.position - _player.transform.position).normalized;
            Action();
            shots--;

            yield return new WaitWhile(() => _bowman.AIpaused);
            yield return new WaitForSeconds(1);
        }

        _bowman.ChangeStatus(Bowmanstatus.Searching);
    }
#endregion
}