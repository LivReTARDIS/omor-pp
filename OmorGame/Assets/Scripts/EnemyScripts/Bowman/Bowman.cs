using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;


public class Bowman : Enemy
{
#region Fields
    public Bowmanstatus currentStatus;
    public Bowmanstatus StartingStatus;
    public UnityEvent statusAttackingEvent;
    public UnityEvent statusIdleEvent;
    public UnityEvent statusMovingEvent;
    public UnityEvent statusPanikEvent;
    public UnityEvent StatusSearchingEvent;
    private UnityEvent _event;
#endregion


#region Public Methods
    public void ChangeStatus(Bowmanstatus newStatus)
    {
        switch (newStatus)
        {
            case Bowmanstatus.Following:
                statusMovingEvent.Invoke();
                animator.SetBool("moving",true);

                break;
            case Bowmanstatus.Shooting:
                statusAttackingEvent?.Invoke();
                animator.SetTrigger("Attack");

                break;
            case Bowmanstatus.Panick:
                statusPanikEvent?.Invoke();
                animator.SetBool("moving", true);
                break;
            case Bowmanstatus.Idle:
                statusPanikEvent.Invoke();
                animator.SetBool("moving", false);
                break;
            case Bowmanstatus.Searching:
                StatusSearchingEvent?.Invoke();
                animator.SetBool("moving", false);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newStatus), newStatus, null);
        }

        currentStatus = newStatus;
    }
    [Button]
    public void DebugChange()
    {
        ChangeStatus(StartingStatus);
    }
#endregion


#region Private Methods
    private void Start()
    {
        ChangeStatus(StartingStatus);
    }
#endregion
}
public enum Bowmanstatus
{
    KeepingDistance,
    Following, // sucht sich eien gegener und der ist dann sein buddy 
    Shooting, // bleibt stehen wo eer ist und schie�t pfeile
    Searching, //steht und sucht anderen boy   
    Panick, //hatte schonmal 
    Idle
}