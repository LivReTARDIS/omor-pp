﻿using System.Collections;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


//[RequireComponent(typeof(TextMeshPro))]
[RequireComponent(typeof(ContentSizeFitter))]
public class FancySpeechBubble : MonoBehaviour
{
#region Fields
    /// <summary>
    ///     Minimum height of background.
    /// </summary>
    public float backgroundMinimumHeight;
    /// <summary>
    ///     Vertical margin (top + bottom) between label and background (OPTIONAL).
    /// </summary>
    public float backgroundVerticalMargin;
    /// <summary>
    ///     The bubble background (OPTIONAL).
    /// </summary>
    public Image bubbleBackground;
    /// <summary>
    ///     Character size animate speed.
    ///     Unit: delta font size / second
    /// </summary>
    public float characterAnimateSpeed = 1000f;
    /// <summary>
    ///     Character start font size.
    /// </summary>
    public int characterStartSize = 1;
#endregion


#region Properies
    /// <summary>
    ///     Processed version of raw text.
    /// </summary>
    public string processedText { get; private set; }
    /// <summary>
    ///     A copy of raw text.
    /// </summary>
    public string rawText { get; private set; }
#endregion


#region Public Methods
    /// <summary>
    ///     Set the label text.
    /// </summary>
    /// <param name="text">Text.</param>
    [Button]
    public void Set(string text)
    {
        StopAllCoroutines();
        StartCoroutine(SetRoutine(text));
    }
    /// <summary>
    ///     Set the label text.
    /// </summary>
    /// <param name="text">Text.</param>
    public IEnumerator SetRoutine(string text)
    {
        rawText = text;

        yield return StartCoroutine(TestFit());
        yield return StartCoroutine(CharacterAnimation());
    }
#endregion


#region Private Methods
    private IEnumerator CharacterAnimation()
    {
        // prepare target
        var label = GetComponent<TextMeshPro>();

        // go through character in processed text
        var prefix = "";
        foreach (var c in processedText)
        {
            // animate character size
            var size = characterStartSize;
            while (size < label.fontSize)
            {
                size += (int) (Time.deltaTime * characterAnimateSpeed);
                size = (int) Mathf.Min(size, label.fontSize);
                label.text = prefix + "<size=" + size + ">" + c + "</size>";

                yield return new WaitForEndOfFrame();
            }

            prefix += c;
        }

        // set processed text
        label.text = processedText;
    }
    /// <summary>
    ///     Test fit candidate text,
    ///     set intended label height,
    ///     generate processed version of the text.
    /// </summary>
    private IEnumerator TestFit()
    {
        // prepare targets
        var label = GetComponent<TextMeshPro>();
        var fitter = GetComponent<ContentSizeFitter>();

        // change label alpha to zero to hide test fit
        var alpha = 1;
        label.color = new Color(label.color.r, label.color.g, label.color.b, 0f);

        // configure fitter and set label text so label can auto resize height
        fitter.horizontalFit = ContentSizeFitter.FitMode.Unconstrained;
        fitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        label.text = rawText;

        // need to wait for a frame before label's height is updated
        yield return new WaitForEndOfFrame();
        // make sure label is anchored to center to measure the correct height
        var totalHeight = label.rectTransform.sizeDelta.y;

        // (OPTIONAL) set bubble background
        if (bubbleBackground != null)
            bubbleBackground.rectTransform.sizeDelta = new Vector2(
                bubbleBackground.rectTransform.sizeDelta.x,
                Mathf.Max(totalHeight + backgroundVerticalMargin, backgroundMinimumHeight));

        // now it's time to test word by word
        processedText = "";
        var buffer = "";
        var line = "";
        var currentHeight = -1f;
        // yes, sorry multiple spaces
        foreach (var word in rawText.Split(' '))
        {
            buffer += word + " ";
            label.text = buffer;

            yield return new WaitForEndOfFrame();
            if (currentHeight < 0f) currentHeight = label.rectTransform.sizeDelta.y;
            if (currentHeight != label.rectTransform.sizeDelta.y)
            {
                currentHeight = label.rectTransform.sizeDelta.y;
                processedText += line.TrimEnd(' ') + "\n";
                line = "";
            }

            line += word + " ";
        }

        processedText += line;

        // prepare fitter and label for character animation
        fitter.horizontalFit = ContentSizeFitter.FitMode.Unconstrained;
        fitter.verticalFit = ContentSizeFitter.FitMode.Unconstrained;
        label.text = "";
        label.rectTransform.sizeDelta = new Vector2(label.rectTransform.sizeDelta.x, totalHeight);
        label.color = new Color(label.color.r, label.color.g, label.color.b, 1);
    }
#endregion
}